package radojcic.ssps_suply.widget

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.util.TypedValue
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import android.widget.RemoteViewsService.RemoteViewsFactory
import androidx.annotation.ColorInt
import radojcic.ssps_suply.R
import radojcic.ssps_suply.formatDate
import radojcic.ssps_suply.model.Day


class ListWidgetService : RemoteViewsService() {
    override fun onGetViewFactory(intent: Intent): RemoteViewsFactory {
        return ListRemoteViewsFactory(
            this.applicationContext,
            intent
        )
    }

    companion object {
        var data: List<Day> = listOf()
    }
}

internal class ListRemoteViewsFactory(context: Context, intent: Intent) :
    RemoteViewsFactory {
    private val mContext: Context = context

    override fun onCreate() {
    }

    override fun onDestroy() {
        ListWidgetService.data = listOf()
    }

    override fun getCount(): Int {
        return ListWidgetService.data.count()
    }

    override fun getViewAt(position: Int): RemoteViews {

        val data = ListWidgetService.data[position]

        // position will always range from 0 to getCount() - 1.
        // We construct a remote views item based on our widget item xml file, and set the
        // text based on the position.

        val rv = RemoteViews(
            mContext.packageName,
            R.layout.widget_view_item
        )

        val mainBackgroundTypedValue = TypedValue()
        val theme: Resources.Theme = mContext.theme
        theme.resolveAttribute(R.attr.colorPrimary, mainBackgroundTypedValue, true)
        @ColorInt val mainBackgroundColor = mainBackgroundTypedValue.data

        rv.setInt(R.id.lineLayout, "setBackgroundColor", mainBackgroundColor)

        rv.setTextViewText(R.id.dateText, data.date?.let { formatDate(it) })

        val dateTextTypedValue = TypedValue()
        theme.resolveAttribute(android.R.attr.textColorPrimary, dateTextTypedValue, true)
        @ColorInt val dateTextColor = dateTextTypedValue.data

        rv.setTextColor(R.id.dateText, dateTextColor)

        for (str in data.events!!.iterator()) {
            val eventTextRv =
                RemoteViews(
                    mContext.packageName,
                    R.layout.widget_item_event_text
                )

            eventTextRv.setTextViewText(R.id.eventText, str)

            val textTypedValue = TypedValue()
            theme.resolveAttribute(android.R.attr.textColorSecondary, textTypedValue, true)
            @ColorInt val eventTextColor = textTypedValue.data

            eventTextRv.setTextColor(R.id.eventText, eventTextColor)

            rv.addView(R.id.tertiaryLayout, eventTextRv)
        }

        // Return the remote views object.
        return rv
    }

    override fun getLoadingView(): RemoteViews? {
        // You can create a custom loading view (for instance when getViewAt() is slow.) If you
        // return null here, you will get the default loading view.
        return null
    }

    override fun getViewTypeCount(): Int {
        return 1
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun onDataSetChanged() {
        // This is triggered when you call AppWidgetManager notifyAppWidgetViewDataChanged
        // on the collection view corresponding to this factory. You can do heaving lifting in
        // here, synchronously. For example, if you need to process an image, fetch something
        // from the network, etc., it is ok to do it here, synchronously. The widget will remain
        // in its current state while work is being done here, so you don't need to worry about
        // locking up the widget.
    }
}