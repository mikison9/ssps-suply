package radojcic.ssps_suply.widget

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.net.Uri
import android.util.TypedValue
import android.widget.RemoteViews
import androidx.annotation.ColorInt
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import radojcic.ssps_suply.R
import radojcic.ssps_suply.formatDate
import radojcic.ssps_suply.logic.DbAccess
import radojcic.ssps_suply.logic.SspsProvider
import radojcic.ssps_suply.model.Day
import radojcic.ssps_suply.model.dbToRegular

class ListWidgetProvider : AppWidgetProvider() {

    override fun onUpdate(
        context: Context?,
        appWidgetManager: AppWidgetManager?,
        appWidgetIds: IntArray?
    ) {
        // update each of the widgets with the remote adapter

        GlobalScope.launch {
            if (appWidgetIds != null && context != null) {

                SspsProvider.fetchToDb(context)

                val data = DbAccess.getDb(context).userDao().getAll().map { dbToRegular(it) }

                ListWidgetService.data = data

                for (id in appWidgetIds) {

                    val intent = Intent(context, ListWidgetService::class.javaObjectType)
                    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id)

                    intent.data = Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME))
                    val rv =
                        RemoteViews(
                            context.packageName,
                            R.layout.widget_layout
                        )

                    when (Resources.getSystem().configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
                        Configuration.UI_MODE_NIGHT_YES -> context.setTheme(R.style.AppTheme_Dark)
                        Configuration.UI_MODE_NIGHT_NO -> context.setTheme(R.style.AppTheme)
                    }

                    val typedValue = TypedValue()
                    val theme: Resources.Theme = context.theme
                    theme.resolveAttribute(R.attr.backgroundColor, typedValue, true)
                    @ColorInt val color = typedValue.data

                    rv.setInt(R.id.widgetMainView, "setBackgroundColor", color)


                    rv.setRemoteAdapter(R.id.widgetMainView, intent)

                    appWidgetManager?.updateAppWidget(id, rv)
                }

                super.onUpdate(context, appWidgetManager, appWidgetIds)
            }
        }
    }

    companion object {
        fun createItemView(data: Day, context: Context): RemoteViews {
            // position will always range from 0 to getCount() - 1.
            // We construct a remote views item based on our widget item xml file, and set the
            // text based on the position.

            val rv = RemoteViews(
                context.packageName,
                R.layout.main_view_item
            )

            rv.setTextViewText(
                R.id.dateText, data.date?.let {
                    formatDate(
                        it
                    )
                }
            )

            for (str in data.events!!.iterator()) {
                val eventTextRv = RemoteViews(
                    context.packageName,
                    R.layout.main_item_event_text
                )

                eventTextRv.setTextViewText(R.id.eventText, str)

                rv.addView(R.id.tertiaryLayout, eventTextRv)
            }

            // Return the remote views object.
            return rv
        }
    }
}