package radojcic.ssps_suply

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.format.DateTimeFormatter

fun dayOfWeekToString(day: DayOfWeek): String {
    return when (day) {
        DayOfWeek.MONDAY -> "Pondělí"
        DayOfWeek.TUESDAY -> "Úterý"
        DayOfWeek.WEDNESDAY -> "Středa"
        DayOfWeek.THURSDAY -> "Čtvrtek"
        DayOfWeek.FRIDAY -> "Pátek"
        DayOfWeek.SATURDAY -> "Sobota"
        DayOfWeek.SUNDAY -> "Neděle"
        else -> ""
    }
}

fun formatDate(date: LocalDate): String {
    return date.format(DateTimeFormatter.ofPattern("dd. MM. yyyy"))
}

fun requestDateFormat(date: LocalDate?): String {
    return when (date) {
        null -> ""
        else -> date.format(DateTimeFormatter.ofPattern("yyyyMMdd"))
    }
}
