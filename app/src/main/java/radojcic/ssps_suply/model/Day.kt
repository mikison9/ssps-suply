package radojcic.ssps_suply.model

import androidx.room.*
import radojcic.ssps_suply.requestDateFormat
import java.time.LocalDate

@Entity(tableName = "days")
data class DbDay(
    @PrimaryKey(autoGenerate = true) val uid: Int?,
    //format stejny jako pro access k api
    @ColumnInfo(name = "date") val date: String,
    @ColumnInfo(name = "events") val events: String
)

@Dao
interface DbDayDao {
    @Query("SELECT * FROM days")
    suspend fun getAll(): List<DbDay>

    @Query("SELECT * FROM days WHERE uid IN (:userIds)")
    suspend fun loadAllByIds(userIds: IntArray): List<DbDay>

    @Query("SELECT * FROM days WHERE date LIKE :date LIMIT 1")
    suspend fun findByDate(date: String): DbDay

    @Query("DELETE FROM days")
    suspend fun deleteAll()

    @Insert
    suspend fun insertAll(vararg users: DbDay)

    @Delete
    suspend fun delete(user: DbDay)
}

@Database(entities = arrayOf(DbDay::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): DbDayDao
}

data class Day(
    val date: LocalDate?,
    val events: List<String>?
)

fun dbToRegular(dbDay: DbDay): Day {
    val dateStr = dbDay.date
    return Day(
        LocalDate.of(
            dateStr.substring(0, 4).toInt(),
            dateStr.substring(4, 6).toInt(),
            dateStr.substring(6, 8).toInt()
        ), dbDay.events.split(";;")
    )
}

fun regularToDb(day: Day): DbDay {
    return DbDay(null, requestDateFormat(day.date), day.events?.joinToString(";;").toString())
}