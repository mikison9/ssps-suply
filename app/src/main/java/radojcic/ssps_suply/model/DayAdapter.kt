package radojcic.ssps_suply.model

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView
import radojcic.ssps_suply.R
import radojcic.ssps_suply.dayOfWeekToString
import radojcic.ssps_suply.formatDate

class DayAdapter(private var myDataset: List<Day>) :
    RecyclerView.Adapter<DayAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView: View =
            LayoutInflater.from(parent.context).inflate(R.layout.main_view_item, parent, false)


        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = myDataset[position]

        val mainLayout: ViewGroup = ((holder.view as ViewGroup).children.first {
            it.id == R.id.mainLayout
        } as ViewGroup)

        val dateText = (mainLayout.children.first {
            it.id == R.id.dateText
        } as TextView)

        val dateStr = data.date?.let { formatDate(it) }
        val dayName = data.date?.dayOfWeek?.let {
            dayOfWeekToString(
                it
            )
        }

        dateText.text = "$dayName - $dateStr"

        val tertiaryLayout: ViewGroup = ((mainLayout.children.first {
            it.id == R.id.secondaryLayout
        } as ViewGroup).children.first {
            it.id == R.id.tertiaryLayout
        } as ViewGroup)

        val inflater = LayoutInflater.from(tertiaryLayout.context)

        tertiaryLayout.removeAllViews()

        for (str in data.events!!.iterator()) {
            val textView: TextView =
                (inflater.inflate(R.layout.main_item_event_text, tertiaryLayout, false) as TextView)

            textView.text = str

            tertiaryLayout.addView(textView)
        }
    }

    override fun getItemCount(): Int {
        return myDataset.count()
    }

    fun updateList(data: List<Day>) {
        myDataset = data
        notifyDataSetChanged()
    }

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}

