package radojcic.ssps_suply

import android.app.Activity
import android.content.res.Configuration
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.main_activity_layout.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import radojcic.ssps_suply.logic.DbAccess
import radojcic.ssps_suply.logic.SspsProvider
import radojcic.ssps_suply.model.DayAdapter

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_YES -> setTheme(R.style.AppTheme_Dark)
            Configuration.UI_MODE_NIGHT_NO -> setTheme(R.style.AppTheme)
        }

        setContentView(R.layout.main_activity_layout)

        GlobalScope.launch {
            DbAccess.getDb(applicationContext)
        }

        adapter = DayAdapter(mutableListOf())

        val layoutManager = LinearLayoutManager(this)

        val rView = findViewById<RecyclerView>(R.id.recyclerView)

        rView.layoutManager = layoutManager
        rView.adapter = adapter

        fab.setOnClickListener { view ->
            GlobalScope.launch {
                SspsProvider.updateAdapterData(applicationContext) { runOnUiThread(it) }
            }
        }

        GlobalScope.launch {
            SspsProvider.updateAdapterData(applicationContext) { runOnUiThread(it) }
        }
    }


    companion object {
        var adapter: DayAdapter? = null
    }
}
