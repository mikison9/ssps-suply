package radojcic.ssps_suply.logic

import android.content.Context
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.coroutines.awaitStringResponse
import org.json.JSONObject
import radojcic.ssps_suply.MainActivity
import radojcic.ssps_suply.model.Day
import radojcic.ssps_suply.model.regularToDb
import radojcic.ssps_suply.requestDateFormat
import java.time.LocalDate

class SspsProvider {
    companion object {
        //val queue = Volley.newRequestQueue(MainActivity.getAppContext())
        //val _url_deprecated = "https://ssps.cz/api/messages"

        private const val url =
            "https://www.ssps.cz/wp-content/themes/ssps-wordpress-theme/supplementation.php/?date="

        suspend fun fetchToDb(context: Context) {
            val days: MutableList<Day> = mutableListOf()

            val now = LocalDate.now()
            for (x in 0 until 7) {
                val date = now.plusDays(x.toLong())
                val day = _fetchDate(context, date)

                if (day.events != null && day.events.isNotEmpty()) {
                    days.add(day)
                }
            }

            val db = DbAccess.getDb(context)

            db.userDao().deleteAll()

            db.userDao().insertAll(*days.map { regularToDb(it) }.toTypedArray())
        }


        suspend fun updateAdapterData(context: Context, runOnUiCallback: (Runnable) -> Unit) {
            val days: MutableList<Day> = mutableListOf()

            val now = LocalDate.now()
            for (x in 0 until 7) {
                val date = now.plusDays(x.toLong())
                val day = _fetchDate(context, date)

                if (day.events != null && day.events.isNotEmpty()) {
                    days.add(day)
                }
            }

            runOnUiCallback(
                Runnable {
                    MainActivity.adapter?.updateList(days)
                }
            )

            val db = DbAccess.getDb(context)

            db.userDao().deleteAll()

            db.userDao().insertAll(*days.map { regularToDb(it) }.toTypedArray())
        }

        private suspend fun _fetchDate(
            context: Context,
            date: LocalDate
        ): Day {
            //val url = URL(url + requestDateFormat(date))

            val resp = Fuel.get(url + requestDateFormat(date)).awaitStringResponse()

            if (resp.second.statusCode != 200) {
                error("Assertion failed")
            } else {
                return SspsParser.parse(resp.third, date)
            }
        }
    }
}

//private val ns: String? = null

class SspsParser {
    companion object {
        fun parse(str: String, date: LocalDate): Day {
            val json = JSONObject(str)
            val classes = json.getJSONArray("ChangesForClasses")

            val events: MutableList<String> = mutableListOf()

            for (x in 0 until classes.length()) {
                val cls = classes.getJSONObject(x)
                val className = cls.getJSONObject("Class").getString("Abbrev")

                val changedLessons = cls.getJSONArray("ChangedLessons")

                val tmpEvents: MutableList<String> = mutableListOf()

                // changed lessons
                for (y in 0 until changedLessons.length()) {
                    val eventJson = changedLessons.getJSONObject(y)
                    val chgType1 = eventJson.getString("ChgType1")
                    val chgType2 = eventJson.getString("ChgType2")
                    val hour = eventJson.getString("Hour")
                    val subject = eventJson.getString("Subject")
                    val group = eventJson.getString("Group")
                    val room = eventJson.getString("Room")
                    val teacher = eventJson.getString("Teacher")

                    val event =
                        "$className - $hour. hod $subject $chgType2 $chgType1 ($teacher) v učebně $room"

                    tmpEvents.add(event)
                }

                val cancelledLessons = cls.getJSONArray("CancelledLessons")

                //cancelledLessons
                for (y in 0 until cancelledLessons.length()) {
                    val eventJson = cancelledLessons.getJSONObject(y)
                    val chgType1 = eventJson.getString("ChgType1")
                    val chgType2 = eventJson.getString("ChgType2")
                    val hour = eventJson.getString("Hour")
                    val subject = eventJson.getString("Subject")
                    val group = eventJson.getString("Group")

                    val event = "$className - $hour. hod $subject $chgType1 $chgType2"

                    tmpEvents.add(event)
                }

                val changedGroups = cls.getJSONArray("ChangedGroups")

                for (y in 0 until changedGroups.length()) {
                    val event = changedGroups.getString(y)

                    tmpEvents.add("$className - $event")
                }

                tmpEvents.sortDescending()

                events.addAll(tmpEvents)

            }

            return Day(date, events)
        }

        /*
        fun _parse_deprecated(inputStream: InputStream): List<Day> {
            inputStream.use { inputStream ->
                val parser: XmlPullParser = Xml.newPullParser()
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
                parser.setInput(inputStream, null)
                parser.nextTag()

                return readFeed(
                    parser
                )
            }

        }

        @Throws(XmlPullParserException::class, IOException::class)
        private fun readFeed(parser: XmlPullParser): List<Day> {
            val entries = mutableListOf<Day>()

            while (parser.next() != XmlPullParser.END_DOCUMENT) {
                if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }

                if (parser.name == "message") {
                    entries.add(
                        readMessage(
                            parser
                        )
                    )
                } else {
                    skip(parser)
                }
            }

            return entries
        }

        @Throws(XmlPullParserException::class, IOException::class)
        private fun readMessage(parser: XmlPullParser): Day {
            parser.require(
                XmlPullParser.START_TAG,
                ns, "message"
            )
            var date: LocalDate? = null
            var events: List<String>? = null

            while (parser.next() != XmlPullParser.END_TAG && parser.name != "message") {

                if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }

                when (parser.name) {
                    "date_start" -> date =
                        readDate(
                            parser
                        )
                    "content" -> events =
                        readEvents(
                            parser
                        )
                    else -> skip(
                        parser
                    )
                }
            }
            return Day(date, events)

        }

        @Throws(IOException::class, XmlPullParserException::class)
        private fun readEvents(parser: XmlPullParser): List<String>? {

            parser.require(
                XmlPullParser.START_TAG,
                ns, "content"
            )

            val text =
                readText(parser)

            if (text.contains("li")) {
                return readEventsAleDoopravdy(
                    text.byteInputStream()
                )
            } else {

                return listOf(Jsoup.parse(text).text())
            }
        }

        @Throws(IOException::class, XmlPullParserException::class)
        private fun readEventsAleDoopravdy(inputStream: InputStream): List<String>? {
            inputStream.use { inputStream ->
                val events = mutableListOf<String>()
                val parser: XmlPullParser = Xml.newPullParser()
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
                parser.setInput(inputStream, null)
                parser.nextTag()

                parser.require(
                    XmlPullParser.START_TAG,
                    ns, "ul"
                )

                parser.nextTag()

                while (parser.name == "li") {
                    val text =
                        readText(
                            parser
                        )
                    events.add(text)

                    parser.nextTag()
                }

                return events
            }
        }

        @Throws(IOException::class, XmlPullParserException::class)
        private fun readDate(parser: XmlPullParser): LocalDate? {
            var date: LocalDate? = null

            parser.require(
                XmlPullParser.START_TAG,
                ns, "date_start"
            )

            val text =
                readText(parser)

            val parts = text.split('.')

            date = LocalDate.of(
                parts[2].trim().toInt(),
                parts[1].trim().toInt(),
                parts[0].trim().toInt()
            )

            parser.require(XmlPullParser.END_TAG, ns, "date_start")


            return date
        }

        @Throws(IOException::class, XmlPullParserException::class)
        private fun readText(parser: XmlPullParser): String {
            var result = ""
            if (parser.next() == XmlPullParser.TEXT) {
                result = parser.text
                parser.nextTag()
            }
            return result
        }


        @Throws(XmlPullParserException::class, IOException::class)
        private fun skip(parser: XmlPullParser) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                throw IllegalStateException()
            }
            var depth = 1
            while (depth != 0) {
                when (parser.next()) {
                    XmlPullParser.END_TAG -> depth--
                    XmlPullParser.START_TAG -> depth++
                }
            }
        }
         */
    }
}