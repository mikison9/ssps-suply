package radojcic.ssps_suply.logic

import android.content.Context
import androidx.room.Room
import radojcic.ssps_suply.model.AppDatabase

class DbAccess {
    companion object {
        private var db: AppDatabase? = null
        private const val dbFile = "db"

        suspend fun getDb(context: Context): AppDatabase {
            if (db == null) {
                db = Room.databaseBuilder<AppDatabase>(
                    context,
                    AppDatabase::class.java, dbFile
                ).build()
            }
            return this.db!!
        }
    }
}